# DANIO-CODE ChIP-seq pipeline v1.0

Chromatin immunoprecipitation coupled with high throughput sequencing (ChIP-seq)
is the most widely used assay for genome-wide protein-DNA interaction detection,
notably transcription factors and histone modifications. As the name suggests, 
the method is based on sequencing DNA fragments obtained after enrichment with 
chromatin immunoprecipitation (Johnson et al., 2007)

The pipeline is based on [AQUAS ChIP-seq pipeline](https://github.com/kundajelab/chipseq_pipeline) and [ENCODE ChIP-seq pipeline](https://github.com/ENCODE-DCC/chip-seq-pipeline2)

## Description of the pipeline

The ChIP-seq pipeline is still a work in progress, and unfortunately doesn’t 
include the final data processing step (peak calling and quality control). 
Nevertheless, the consortium decided to make the first part of the processed 
data available. This includes mapped reads as well as strand cross-correlation 
analysis. 

## Pipeline workflow

The following list describes the workflow, each step with it's inputs and outputs:

1. Aligning reads (bwa): Aligning raw reads to the reference genome
   1. Inputs:
      1. Reads in fastq format
      2. Compressed genome index
   2. Outputs:
      1. BAM file of the aligned reads (*.bam)
      2. Quality metrics from `samtools flagstat` (*_qc.txt)
2. Filtering of aligned reads: Filtering the ba file for unaligned reads and 
   and duplicates with `samtools` and `sambamba`
   1. Inputs:
      1. Bam file (1.2.1)
   2. Outputs:
      1. Filtered bam file (*.filtered.bam) 
3. Optical duplicates detection: Detect optical duplicates with Picard tools
   1. Inputs:
      1. Bam file (2.2.1)
   2. Outputs:
      1. Bam file 
4. Duplicates removal: Second round of filtering with `samtools` and `sambamba`
   1. Inputs:
      1. Bam file (3.2.1)
   2. Outputs:
      1. Bam file (*_nodups.bam) 
5. Cross-correlation analysis
   1. Inputs:
      1. Bam file (4.2.1)
   2. Outputs:
      1. Cross-correlation quality metrics (*.cc.qc)
      2. Cross-correlation plots (*.cc.pdf)

## Instructions to run

To run the pipeline, the Apps need to be buit on DNAnexus. The `dx-toolkit` is
required for this. The Apps can be built by running `dx build` from the App 
folder. 

The pipeline can be run by constructing a workflow which follows the described scheme.
Here is the list of Apps which perform each step:

1. Aligning reads:
   * `ChIP-seq-align` for single-end reads
   * `chip-seq-align-se-pe` for single-end and paired-end reads, depending if
   if the second fastq is provided
2. Filtering aligned reads
   * `chip-seq-dedup-1`
3. Optical duplicates detection
   `Picard MarkDuplicates Mappings Deduplicator` --> publicly available tool
4. Duplicates removal
   * `chip-seq-dedup-2`
5. Cross-correlation analysis
   * `xcor`

Although all the dependencies are provided with the Apps, in this repository 
Apps for bulding, compailing and installing required tools are also included (
`samtools_install`, `install_bedtools`). Assets should also be created (`ChIPasset`,
`ChIPasset_xcor`) and the `record_id` in the dxapp.json which uses the asset should be changed
(`ChIPasset_xcor` for `xcor` App). The genome index should be constructed with 
the `bwa-genome-index` App.

## Update

Deduplicated bam files were first converted to [tagAlign format](http://genome.ucsc.edu/FAQ/FAQformat.html#format15).
The pipelines are now run on local servers, following the ENCODE running protocol. Replicates were used as a quality control check when available, otherwise pseudo-replicates were used for quality check.
ChIP-seq Inputs with sufficient sequencing depth were used as background control for calling peaks where available (-c option in macs2).
Peaks were called using MACS2 with the following parameters:

`macs2 callpeak -t <sample.tagAlign.gz> -c <control.tagAlign.gz> -f BED -g 1371719383 -p 0.01 --nomodel --shift 0 --extsize <calculated cross-correlation value> --keep-dup all -B --SPMR`

The outputs of the the peak caller were peak files in `narrowPeak` format, background signal file, and a normalised sample signal file, which were used with the command `macs2 bdgcmp -m ppois`
